const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');

const SalgadoSchema = new mongoose.Schema({
    nome : {
        type : String,
        required : true
    },
    tipo: {
        type: String,
        required: true
    },
    recheio:{
        type: String,
        required: true
    }
})

module.exports = mongoose.model('salgados', SalgadoSchema);
