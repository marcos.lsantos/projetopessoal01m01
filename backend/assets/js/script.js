const urlServer = "http://localhost3000/user"

const tbody = document.querySelector('#list table tbody')

const createElement = (item) =>{
    const elementTr = document.createElement('tr')
    elementTr.innerHTML = `
        <td>${item.name}</td>
        <td>${item.tipo}</td>
        <td>${item.recheio}</td>
    `

    return elementTr
}

async function getList(){
    const result = await fetch(urlServer)
    const data = await result.json

    data.map(item => tbody.appendChild(createElement(item)))
}