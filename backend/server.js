const express = require('express')
var bodyParser = require('body-parser')
const connectDB = require('./config/db')
const app = express()
const cors = require('cors')
const PORT = 3003

// Init Middleware
app.use(express.json())
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(cors())

//Connect Database
connectDB()

// Define Routes
app.use('/', require('./routes/hello'))
app.use('/user', require('./routes/api/user'))
app.use('/cadsalgados', require('./routes/api/cadsalgados'))

app.listen(PORT, () => { console.log(`port ${PORT}`) })