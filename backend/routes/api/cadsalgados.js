const express = require ('express')
const router = express.Router()
const Salgados = require('../../models/salgados');
const {check, validationResult} = require('express-validator');
const { findOneAndUpdate } = require('../../models/salgados');
const salgados = require('../../models/salgados');
const { route } = require('./user');


router.get('/',[], async (req, res, next)=>{
    try{
        let salgados = await Salgados.find({})
        res.json(salgados)
    }catch(err){
        console.error(err.message)
        res.status(500).send({"erro": "Server error"})
    }
})


router.post('/', [
], async (req, res, next) => {
    try{
        let { nome, tipo, recheio} = req.body
        let salgados = new Salgados({ nome, tipo, recheio})
        await salgados.save()
        res.json(salgados)
    }catch(err){
        console.error(err.message)
        res.status(500).send({"erro": "Server error"})
    }
})


 
router.patch('/:nome', [], async (req, res, next)=>{
    try{
        let nomeBusca = req.params.nome
        let update = {$set: req.body}

        let salgados = await Salgados.findOneAndUpdate({nome: nomeBusca}, update, {new: true})

        res.json(salgados)
    }catch(err){
        console.error(err.message)
        res.status(500).send({"erro": "Server error"})
    }
})

router.delete('/:nome', [], async (req, res, next)=>{
    try{
        let nomeBusca = req.params.nome
        let salgado = await salgados.findOneAndDelete({nome: nomeBusca})
        res.json(salgado)
    }catch(err){
        console.error(err.message)
        res.status(500).send({"erro": "Server error"})
    }
})

module.exports = router