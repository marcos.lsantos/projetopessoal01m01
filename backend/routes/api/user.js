const express = require('express')
const router = express.Router();
const bcrypt = require('bcryptjs');
const User = require('../../models/user');
const {check, validationResult} = require('express-validator');

router.get('/', async(req, res, next)=>{
    try{
        const user = await User.find({})
        res.json(user)
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error": "Server Error"})
    }
})


router.post('/', [
    check('nome').not().isEmpty(),
    check('email').not().isEmpty(),
    check('senha').not().isEmpty()
], async(req, res, next) => {
    try{
        let {nome, email, senha} = req.body
        const errors = validationResult(req)
         
        if(!errors.isEmpty()){
            return res.status(400).json({errors: errors.array()})
        }else{
            let usuario = new User({nome, email, senha})

            const salt = await bcrypt.genSalt(10);
            usuario.senha = await bcrypt.hash(senha, salt);

            await usuario.save()
            const payload = {
                usuario: {
                    id: usuario.id
                }
            }

            if(usuario.id){
                res.json(usuario)
            }
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }


})

router.patch('/:email', [
], async(req, res, next) => {
    try{
        const email = req.params.email
        const salt = await bcrypt.genSalt(10)

        if(req.body.senha){
            req.body.senha = await bcrypt.hash(req.body.senha, salt)
        }

        let update = {$set: req.body}

        const user  = await User.findOneAndUpdate(email, update, { new: true })

        if(user){
            res.json(user)
        }else{
            res.status(404).send({"error": "user not found"})
        }

    }catch(err){
        console.error(err.message)
        res.status(500).send({"error": "Server error"})
    }
})

router.delete('/:email', async(req, res, next) => {
    try {
      let param_email = req.params["email"]
      const user = await User.findOneAndDelete({ email: param_email })
      if (user) {
        res.json(user)
      } else {
        res.status(404).send({ "error": "user not found" })
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": "Server Error" })
    }
  })


module.exports = router