import React from 'react';
import Footer from '../src/components/layout/footer'
import Header from '../src/components/layout/header'
import Router from './routes'
import './index.css';


function App() {

  return (
    <section>
      <div>
      <Header/>
      <Router/> 
      <Footer />
      </div>
    </section>
  );
}

export default App;
