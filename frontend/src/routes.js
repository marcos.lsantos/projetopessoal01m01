/* import List from './components/salgados/list'

const routes = { 
    'List': {
        component: List,
        showMenu: true,
        actions: {
            name: 'Novo',
            to: "Create"
        }
    }
 
}

export default routes
 */
import React from 'react'
import ListSalgados from './components/salgados/list'
import CreateSalgados from './components/salgados/cadastro'
import Login from './components/login/login'
import {
    BrowserRouter as Router,
    Switch,
    Route
    // Redirect
} from "react-router-dom";


const Routers = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/create" component={CreateSalgados} />
            <Route exact path="/list" component={ListSalgados} />
            {/* <Route exact path="*" component={() => (<Redirect to="/" />)} /> */}
            <Route exact path="*" component={() => (<h1>404 | Not Found</h1>)} />

        </Switch>
    </Router>
)

export default Routers;
