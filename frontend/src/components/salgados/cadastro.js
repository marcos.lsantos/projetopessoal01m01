import React, { useState } from 'react'
import { createSalgados } from '../../service/salgados'
import {useHistory} from 'react-router-dom'
import './style.css'


const Cadastro = ()=>{

    const [dados, setDados] = useState({})
    const history = useHistory()

    const redirectPage = () => {
        history.push("/list")
    }

    const guardarDados = (conteudo)=>{      
        setDados({
            ...dados,
            [conteudo.target.name]: conteudo.target.value
        })
        return
    }
    
    const enviarDados = async()=>{
        await createSalgados(dados)
        alert("Cadastro efetuado com sucesso")
        redirectPage()
    }    

    return(
        <section>
            <div className="container">
            <form className="caixa">
                <div className="rotulo" >
                    <label >Nome</label><br/>
                </div>
                <div className="rotulo">
                    <input type="text" name="nome" value={dados.nome} onChange={guardarDados}/>
                </div>
                <div className="rotulo">
                    <label >Tipo</label><br/>
                    </div>
                <div className="rotulo">
                    <input  type="text" name="tipo" value={dados.tipo} onChange={guardarDados}/>
                </div>
                <div className="rotulo">
                    <label >Recheio</label><br/>
                </div>
                <div className="rotulo">
                    <input  type="text" name="recheio" value={dados.recheio} onChange={guardarDados}/>
                </div>
                <div className="rotulo">
                    <button className="btn" onClick={enviarDados}>Gravar</button>
                    <button className="btn" onClick={redirectPage}>Lista</button>
                </div>
            </form>
            </div>
        </section>
    )
}

export default Cadastro