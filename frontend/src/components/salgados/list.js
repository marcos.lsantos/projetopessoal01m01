import React, { useState, useEffect } from 'react'
import {listSalgados, deletSalgados} from '../../service/salgados'
import {useHistory} from 'react-router-dom'
import './style.css'



const Lista =  ()=>{
    const [salgado, setSalgados] = useState([])

    const getSalgados = async () => {
        const salgados = await listSalgados()
        setSalgados(salgados.data)
       
    }

    const history = useHistory()
    const changePage = () => {
        history.push("/create")
    }

   const verifyIsEmpty = salgado.length === 0

     const editar = async (codigo) =>{
        console.log('Funcionando')
        //await modifySalgados(`nome`)
    }

     const excluir = async ({nome}) =>{
        try {
            if (window.confirm(`Voce deseja excluir o salgado ${nome}`)) {
                await deletSalgados(nome)
                getSalgados()
            }
        } catch (error) {
            console.log(error)
        }
      
        
    }
 
    

    const montarTabela =()=>{
       
          const conteudo = salgado.map((salgadinhos, index) => (
            <tr key={index}>
                <td>{salgadinhos.nome}</td>
                <td>{salgadinhos.tipo}</td>
                <td>{salgadinhos.recheio}</td>
                <td>
                    <span onClick={() => editar(salgadinhos)} >Editar</span> |
                    <span onClick={() => excluir(salgadinhos)}>   Excluir </span>
                </td>
            </tr>
          )
    )
    
    return (
        <section> 
            <div>
                {!verifyIsEmpty ?(
                    <div>
                        <nav className="">
                            <div className="title"> <h1>Lista de Salgados</h1></div>
                            <div className="action">
                                <button onClick={changePage}> Novo</button>
                            </div>
                        </nav>                    
                        <table>
                            <thead>
                                <tr>
                                    <td>Nome</td>
                                    <td>Tipo</td>
                                    <td>Recheio</td>
                                    <td>Procedimentos</td>
                                </tr>
                            </thead>
                            <tbody>
                                {conteudo}
                            </tbody>
                        </table>
                    </div>
                ): ""
                }
            </div>
        </section>
    )
    }
    useEffect(()=>{
        getSalgados()      
    },[])

    return(
        <div>
            <div>
                {montarTabela()}
            </div>
        </div>
    )
}

export default Lista