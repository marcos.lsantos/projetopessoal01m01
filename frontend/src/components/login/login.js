import React, {useState} from 'react'
import { useHistory } from 'react-router-dom';
import './style.css'
import logar from '../../service/user'



const Main = (props) => {
   
    const [dados, setLogar] = useState({})

    const history = useHistory()
    const changePage = () => {
        history.push("/list")
    }

    const logar = (conteudo)=>{
        setLogar({
            ...dados,
            [conteudo.target.name]: conteudo.target.value
        })
        return
    }

  

    return(
    <section>
        <div className="container">
            <div className="caixa">
                <div className="rotulo">
                    <label >Email</label><br/>
                </div>
                <div className="conteudo">
                    <input type="text" name="email" value={dados.email} onChange={logar}/>
                </div>
                <div className="rotulo">
                    <label >Senha</label><br/>
                </div>
                <div className="conteudo">
                    <input type="password" name="senha" value={dados.senha} onChange={logar}/>
                </div>
                <div className="btnplus">
                    <br/>
                    <button className="btn" onClick={changePage}>Entrar</button>
                    <span >Cadastrar-se</span>
                </div>
            </div>
        </div>
    </section>

    )
}

export default Main



