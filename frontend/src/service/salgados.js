import {clientHttp} from '../config/config'

const createSalgados = (dados)=> clientHttp.post (`/cadsalgados`, dados)

const listSalgados = ()=> clientHttp.get(`/cadsalgados`)

const deletSalgados = (nome) => clientHttp.delete(`/cadsalgados/${nome}`)

const modifySalgados = () => clientHttp.patch(`/cadsalgados/:nome`)

export{
    createSalgados,
    listSalgados,
    deletSalgados,
    modifySalgados
}