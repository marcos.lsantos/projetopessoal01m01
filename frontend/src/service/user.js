import {clientHttp} from '../config/config'

const createUser = (dados)=> clientHttp.post (`/user`, dados)

const logar = (email) => clientHttp.get(`/user/${email}`)

export{
    createUser,
    logar
}